FOR /R %%G IN (*.gif) DO ffmpeg -i "%%~dpnxG" -f apng -plays 0 "%%~dpnG.apng"
FOR /R %%G IN (*.apng) DO MOVE "%%~dpnxG" "%%~dpnG.apng_temp"
FOR /R %%G IN (*.apng_temp) DO apngopt "%%~dpnxG" "%%~dpnG.apng"
DEL /s /q /f  *.gif
DEL /s /q /f  *.apng_temp